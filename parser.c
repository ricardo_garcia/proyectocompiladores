#include "parser2.h"
#include <stdio.h>
extern token *yylex();
extern int yylineno;

void eat(int clase) {
  printf("-CLASE %i \n",clase);
  printf("-Token actual %i \n",tokenActual->clase);
  if(equals(tokenActual, clase)) {
    tokenActual = yylex();
  } else {
    error("Error de sintaxis EAT");
  }
}

void parse() { 
  PROGRAMA(); 
}

void PROGRAMA() {
  DECLARACIONES();
  FUNCIONES();
  if(equals(tokenActual, FIN)) {
    printf("final %i",tokenActual->clase);
    puts("Fin de análisis sintáctico.");
    return;
  }
}

void DECLARACIONES(){
  if (equals(tokenActual,INT) || 
  equals(tokenActual,CHAR) ||
  equals(tokenActual,FLOAT) ||
  equals(tokenActual,DOUBLE) ||
  equals(tokenActual,VOID)|| 
  equals(tokenActual, PARI)){
    printf("Revisamos Tipo\n");
    TIPO();
  }
  if(equals(tokenActual,IDS)){
    printf("Revisamos LISTA_VAR\n");
    LISTA_VAR();
  }
  if(equals(tokenActual,PCOMA)){
    printf("Revisamos PCOMA\n");
    eat(PCOMA);
  }
   if (equals(tokenActual,INT) || 
  equals(tokenActual,CHAR) ||
  equals(tokenActual,FLOAT) ||
  equals(tokenActual,DOUBLE) ||
  equals(tokenActual,VOID)|| 
  equals(tokenActual, PARI)){
    DECLARACIONES();
  }
}
void TIPO(){
  printf("\ntipo: %i",tokenActual->clase);
  if (equals(tokenActual,INT) || 
  equals(tokenActual,CHAR) ||
  equals(tokenActual,FLOAT) ||
  equals(tokenActual,DOUBLE) ||
  equals(tokenActual,VOID) 
  )
  {
    printf("Básico\n");
    BASICO();
  }
  
  if(equals(tokenActual, PARI)){
    COMPUESTO();
  }

}
void BASICO(){
  switch (tokenActual->clase)
  {
  case INT:
    /* code */
    if(equals(tokenActual,INT))
      eat(INT);
    break;
  case FLOAT:
    /* code */
    if(equals(tokenActual,FLOAT))
      eat(FLOAT);
    printf("FLOAT\n");
    break;
  case CHAR:
    /* code */
  if(equals(tokenActual,CHAR))  
    eat(CHAR);
    break;
  case DOUBLE:
    /* code */
  if(equals(tokenActual,DOUBLE))
    eat(DOUBLE);
    break;
  case VOID:
    /* code */
  if(equals(tokenActual,VOID))
    eat(VOID);
    break;
  
  }
}
void COMPUESTO(){

  switch (tokenActual->clase)
  {
  case PARI:
    if(equals(tokenActual,PARI))
      eat(PARI);
    if(equals(tokenActual,NUM))
      eat(NUM);
    if(equals(tokenActual,PARD))
      eat(PARD);

    if(equals(tokenActual,PARI))
      COMPUESTO();
    break;
  default:
    break;
  }

}
//lista_var -> lista_var , id | id
/*
*Eliminando recursividad:
*lista_var=A
*id=B
*lista_var -> id lista_var2
* lista_var2 -> ,id lista_var2 | eps
*/
void LISTA_VAR(){
    if(equals(tokenActual, IDS)){
      printf("\nLISTA_VAR \n");
      printf("eat id\n");
      if(equals(tokenActual,IDS))
        eat(IDS);
      if(equals(tokenActual,COMA)){
        LISTA_VAR2();
      }
    }
    
    
}

void LISTA_VAR2(){
  if(equals(tokenActual,COMA)){
    eat(COMA);
  }
  if(equals(tokenActual,IDS))
    eat(IDS);
  if(equals(tokenActual,COMA))
    LISTA_VAR2();
  
}

void FUNCIONES(){
  printf("funciones\n");
  if(equals(tokenActual,FUNC))  
    eat(FUNC);
  if(equals(tokenActual,IDS))  
    eat(IDS);
  if(equals(tokenActual,PARI))      
    eat(PARI);
  
  //first(tipo)
  if (equals(tokenActual,INT) || 
  equals(tokenActual,CHAR) ||
  equals(tokenActual,FLOAT) ||
  equals(tokenActual,DOUBLE) ||
  equals(tokenActual,VOID) 
  ){
    ARGUMENTOS();
  }
  if(equals(tokenActual,PARD))      
    eat(PARD);
  if(equals(tokenActual,LLAVEIZQ))
    BLOQUE();
  if(equals(tokenActual,FUNC))  
    FUNCIONES();
}
void ARGUMENTOS(){
  LISTA_ARGS();
}
/*
*lista args -> lista args, tipo id | tipo id
*ELIMINANDO recursividad
*lista_args -> tipo id lista_args2
*lista_args2 -> , tipo id lista_args2
*/
void LISTA_ARGS(){
  if (equals(tokenActual,INT) || 
  equals(tokenActual,CHAR) ||
  equals(tokenActual,FLOAT) ||
  equals(tokenActual,DOUBLE) ||
  equals(tokenActual,VOID) 
  ){
    TIPO();
  }
  if(equals(tokenActual,IDS)){
    eat(IDS);
    if(equals(tokenActual,COMA))
      LISTA_ARGS2();
  }
}
void LISTA_ARGS2(){
  if(equals(tokenActual,COMA))
    eat(COMA);
  if (equals(tokenActual,INT) || 
  equals(tokenActual,CHAR) ||
  equals(tokenActual,FLOAT) ||
  equals(tokenActual,DOUBLE) ||
  equals(tokenActual,VOID) 
  ){
    TIPO();
  }
  if(equals(tokenActual,IDS)){
    eat(IDS);
  }
  if(equals(tokenActual,COMA))
    LISTA_ARGS2();
}
void BLOQUE(){
  if(equals(tokenActual,LLAVEIZQ)){
  printf("bloque!!!!!!!!\n");
    eat(LLAVEIZQ);
  }
  if (equals(tokenActual,INT) || 
  equals(tokenActual,CHAR) ||
  equals(tokenActual,FLOAT) ||
  equals(tokenActual,DOUBLE) ||
  equals(tokenActual,VOID)|| 
  equals(tokenActual, PARI)){
    DECLARACIONES();
  }
  //printf("TknAct!!! %i-",tokenActual->clase);
  if(equals(tokenActual, IDS) ||
  equals(tokenActual,IF) ||
  equals(tokenActual, ELSE)||
  equals(tokenActual, WHILE)||
  equals(tokenActual, DO)||
  equals(tokenActual, BREAK)||
  equals(tokenActual, SWITCH)||
  equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)||
  equals(tokenActual, MAYOREQ)||
  equals(tokenActual, MAYORQ)||
  equals(tokenActual, MENOREQ)||
  equals(tokenActual, MENORQ)||
  equals(tokenActual, EQUAL)||
  equals(tokenActual, DIFERENCIA)||
  equals(tokenActual, LLAVEIZQ)
  )
  {
    printf("\nInstrucciones %i\n",tokenActual->clase);
    INSTRUCCIONES();
  }
  if(equals(tokenActual,LLAVEDER))
    eat(LLAVEDER);
}
/*
*instrucciones -> instrucciones sentencia | sentencia
*ELIMINANDO recursividad
*instrucciones -> sentencia instrucciones2
*instrucciones2 -> sentencia instrucciones2
*/
void INSTRUCCIONES(){
  // printf("INSTRUCCIONES %i",tokenActual->clase);
  if(equals(tokenActual, IDS) ||
  equals(tokenActual,IF) ||
  equals(tokenActual, ELSE)||
  equals(tokenActual, WHILE)||
  equals(tokenActual, DO)||
  equals(tokenActual, BREAK)||
  equals(tokenActual, SWITCH)||
  equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)||
  equals(tokenActual, MAYOREQ)||
  equals(tokenActual, MAYORQ)||
  equals(tokenActual, MENOREQ)||
  equals(tokenActual, MENORQ)||
  equals(tokenActual, EQUAL)||
  equals(tokenActual, DIFERENCIA)||
  equals(tokenActual,LLAVEIZQ)
  ){

    SENTENCIA();
     if(equals(tokenActual, IDS) ||
    equals(tokenActual,IF) ||
    equals(tokenActual, ELSE)||
    equals(tokenActual, WHILE)||
    equals(tokenActual, DO)||
    equals(tokenActual, BREAK)||
    equals(tokenActual, SWITCH)){
    
     INSTRUCCIONES2();
    }
  }
}
void INSTRUCCIONES2(){
  // SENTENCIA();
  // INSTRUCCIONES2();
  INSTRUCCIONES();
}
void SENTENCIA(){
  switch (tokenActual->clase)
  {
  case IDS:
    if(equals(tokenActual,IDS))  
      LOCALIZACION();
    if(equals(tokenActual,ASIGNACION)){
      eat(ASIGNACION);
    }
    if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){
        BOOLEANO();
    }
    break;
  case IF:
  
  break;
  case WHILE:
    if(equals(tokenActual,WHILE))
      eat(WHILE);
    if(equals(tokenActual,PARI))
      eat(PARI);
    if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){
      printf("-tknWHILE!!!_----->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><--%i",tokenActual->clase);
      BOOLEANO();
    }
      eat(tokenActual->clase);
    if(equals(tokenActual,PARD)){
      eat(PARD);
    }
    
    if(equals(tokenActual,LLAVEIZQ)){
      BLOQUE();
    }
    if(equals(tokenActual, IDS) ||
    equals(tokenActual,IF) ||
    equals(tokenActual, ELSE)||
    equals(tokenActual, WHILE)||
    equals(tokenActual, DO)||
    equals(tokenActual, BREAK)||
    equals(tokenActual, SWITCH)||
    equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)||
    equals(tokenActual,LLAVEIZQ)
    ){
 
    SENTENCIA();
    }
    break;
  case DO:
    if(equals(tokenActual,DO))
      eat(DO);
     if(equals(tokenActual, IDS) ||
      equals(tokenActual,IF) ||
      equals(tokenActual, ELSE)||
      equals(tokenActual, WHILE)||
      equals(tokenActual, DO)||
      equals(tokenActual, BREAK)||
      equals(tokenActual, SWITCH)||
      equals(tokenActual,NEGACION)||
      equals(tokenActual,MENOS)||
      equals(tokenActual,IDS)||
      equals(tokenActual,PARI)||
      equals(tokenActual,NUM)||
      equals(tokenActual,CADENA)||
      equals(tokenActual,TRUE)||
      equals(tokenActual,FALSE)||
      equals(tokenActual, MAYOREQ)||
      equals(tokenActual, MAYORQ)||
      equals(tokenActual, MENOREQ)||
      equals(tokenActual, MENORQ)||
      equals(tokenActual, EQUAL)||
      equals(tokenActual, DIFERENCIA)||
      equals(tokenActual, LLAVEIZQ)
      ){  
        if (equals(tokenActual,LLAVEIZQ))
        {
          BLOQUE();
        }
        else
        {
          SENTENCIA();
        }
        
    }
    if(equals(tokenActual,WHILE))
      eat(WHILE);
    if(equals(tokenActual,PARI))
      eat(PARI);
    if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){   
      BOOLEANO();
    }
    if(equals(tokenActual,PARD))
      eat(PARD);
    break;
  case BREAK:
    printf("BREAK!!!!!! !-%i",tokenActual->clase);
    if(equals(tokenActual,BREAK))
      eat(BREAK);
    if(equals(tokenActual, PCOMA))  
      eat(PCOMA);
    SENTENCIA();
    break;
  case LLAVEIZQ:
    BLOQUE();
    break;
  case SWITCH:
    eat(SWITCH);
    eat(PARI);
    BOOLEANO();
    eat(PARD);
    eat(LLAVEIZQ);
    CASOS();
    eat(LLAVEDER);
  break;
  }


}
void CASOS(){
  switch (tokenActual->clase)
  {
  case CASE:
    CASO();
    CASOS();
    break;
  case DEFAULT:
    PREDETERMINADO();
    break;
  }
}
void CASO(){
  eat(CASE);
  eat(NUM);
  eat(DOSPUNTOS);
  INSTRUCCIONES();
}
void PREDETERMINADO(){
  eat(DEFAULT);
  eat(DOSPUNTOS);
  INSTRUCCIONES();
}
/*
bool -> bool || comb | comb
Eliminamos recursividad
bool -> comb bool2
bool2 -> || comb bool2 
*/
void BOOLEANO(){
  if(equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)||
  equals(tokenActual,MAYOREQ)||
  equals(tokenActual,MAYORQ)||
  equals(tokenActual,MENOREQ)||
  equals(tokenActual,MENORQ)||
  equals(tokenActual,OR)||
  equals(tokenActual,AND)||
  equals(tokenActual,EQUAL)||
  equals(tokenActual,DIFERENCIA)||
  equals(tokenActual,MAS)||
  equals(tokenActual,MULTI)||
  equals(tokenActual,DIV)||
  equals(tokenActual,MODULO)
  ){
  printf("BOOLEANO!!!--%i\n",tokenActual->clase);  
    COMB();
    if(equals(tokenActual,OR))
      BOOLEANO2();
  }

}
void BOOLEANO2(){
  if(equals(tokenActual,OR))
    eat(OR);
  
  // COMB();
  // BOOLEANO2();
if(equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)||
  equals(tokenActual,MAYOREQ)||
  equals(tokenActual,MAYORQ)||
  equals(tokenActual,MENOREQ)||
  equals(tokenActual,MENORQ)||
  equals(tokenActual,OR)||
  equals(tokenActual,AND)||
  equals(tokenActual,EQUAL)||
  equals(tokenActual,DIFERENCIA)||
  equals(tokenActual,MAS)||
  equals(tokenActual,MULTI)||
  equals(tokenActual,DIV)||
  equals(tokenActual,MODULO)
  ){
    printf("BOOLEANO2!!!--%i\n",tokenActual->clase);  
    COMB();
    if(equals(tokenActual,OR))
      BOOLEANO2();
  }
}
/*
* comb -> comb && igualdad | igualdad
**---------------
*comb ->igualdad comb2
*comb2 -> && igualdad comb2
*/
void COMB(){
  if(equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)||
  equals(tokenActual,MAYOREQ)||
  equals(tokenActual,MAYORQ)||
  equals(tokenActual,MENOREQ)||
  equals(tokenActual,MENORQ)||
  equals(tokenActual,OR)||
  equals(tokenActual,AND)||
  equals(tokenActual,EQUAL)||
  equals(tokenActual,DIFERENCIA)||
  equals(tokenActual,MAS)||
  equals(tokenActual,MULTI)||
  equals(tokenActual,DIV)||
  equals(tokenActual,MODULO)
  ){
    IGUALDAD();
    if(equals(tokenActual,AND))
      COMB2();
  }
}
void COMB2(){
  if(equals(tokenActual,AND))
 {   
   eat(AND);
    COMB2();
 }
}
/*
*igualdad -> igualdad == rel | igualdad != rel | rel
*-----------------------
*igualdad -> rel igualdad2
*igualdad2 -> ==rel igualdad2 | != rel igualdad2 | eps
*/
void IGUALDAD(){
    printf("On igualdad %i",tokenActual->clase);
     if(equals(tokenActual,NEGACION)||
        equals(tokenActual,MENOS)||
        equals(tokenActual,IDS)||
        equals(tokenActual,PARI)||
        equals(tokenActual,NUM)||
        equals(tokenActual,CADENA)||
        equals(tokenActual,TRUE)||
        equals(tokenActual,FALSE)){
            eat(tokenActual->clase);
    }

    if( equals(tokenActual,MAYOREQ)||
    equals(tokenActual,MAYORQ)||
    equals(tokenActual,MENOREQ)||
    equals(tokenActual,MENORQ)||
    equals(tokenActual,OR)||
    equals(tokenActual,AND)||
    equals(tokenActual,DIFERENCIA)||
    equals(tokenActual,MAS)||
    equals(tokenActual,MULTI)||
    equals(tokenActual,DIV)||
    equals(tokenActual,MODULO)){
      REL();
    }
    if(equals(tokenActual,EQUAL)){
      IGUALDAD2();
    }
  
}
void IGUALDAD2(){
  switch (tokenActual ->clase)
  {
  case EQUAL:
    printf("EQUAL2\n");
    if(equals(tokenActual,EQUAL))
      eat(EQUAL);
    if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)
    ){
      REL();
      if(equals(tokenActual,EQUAL))
        IGUALDAD2();
    }
    break;
  case DIFERENCIA:
    if(equals(tokenActual,DIFERENCIA))
      eat(DIFERENCIA);
    if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)
    ){
      REL();
      if(equals(tokenActual,DIFERENCIA))
        IGUALDAD2();
    }
    break;
  }
}

void executeExp(){
    if(equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)||
  equals(tokenActual,MAYOREQ)||
  equals(tokenActual,MAYOREQ)||
  equals(tokenActual,MAYORQ)||
  equals(tokenActual,MENOREQ)||
  equals(tokenActual,MENORQ)||
  equals(tokenActual,MAS)||
  equals(tokenActual,MULTI)||
  equals(tokenActual,DIV)||
  equals(tokenActual,MODULO)
  ){ 
    printf("\n On exec %i",tokenActual->clase);
    EXP();
  }
}

void REL(){
  printf("\nOn REL! .........................................................%i\n",tokenActual->clase);
  executeExp();
  // tokenActual = yylex();
  switch (tokenActual->clase)
  {
  case MAYOREQ:
    if(equals(tokenActual,MAYOREQ))
      eat(MAYOREQ);
    // EXP();
    executeExp();
    break;
  case MAYORQ:
    if(equals(tokenActual,MAYORQ))
      eat(MAYORQ);
    // EXP();
    executeExp();
    break;
  case MENOREQ:
    if(equals(tokenActual,MENOREQ))
      eat( MENOREQ);
    // EXP();
    executeExp();
  break;
  case MENORQ:
    if(equals(tokenActual,MENORQ))
      eat(MENORQ);
    // EXP();
    executeExp();
  break;
  
  default:
    break;
  }
}
/*
exp -> exp + term | exp -term | term
-------------
exp  -> term exp2
exp2 -> +term exp2 | -term exp2 | eps
*/
void EXP(){
  if(equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)
  ){ 
    TERM();
    if(equals(tokenActual,MAS) || equals(tokenActual,MENOS))
      EXP2(); 
  }
}

void EXP2(){
  switch (tokenActual->clase)
  {
  case MAS:
    if(equals(tokenActual,MAS))
      eat(MAS);
    // TERM();
    // EXP2();
    EXP();
    break;
  case MENOS:
    if(equals(tokenActual,MENOS))
      eat(MENOS);
    // TERM();
    // EXP2();
    EXP();
    break;
  default:
    break;
  }
}

/*
term -> term ∗ unario | term / unario | term % unario | unario
----------------------
term -> unario term2
term2 -> *unario term2 | / unario term2 |%unario term | eps
*/
void TERM(){
  if(equals(tokenActual,NEGACION)||
  equals(tokenActual,MENOS)||
  equals(tokenActual,IDS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)
  ){ 
    UNARIO();
    if(equals(tokenActual,MULTI)||
    equals(tokenActual,DIV)||
    equals(tokenActual,MODULO)
    ) 
      TERM2();
    }
}
void TERM2(){
  switch (tokenActual->clase)
  {
  case MULTI:
    if(equals(tokenActual,MULTI))
      eat(MULTI);
    // UNARIO();
    // TERM2();
    TERM();
    break;
  case DIV:
    if (equals(tokenActual,DIV))
    {
      eat(DIV);
    }
    // UNARIO();
    // TERM2();
    TERM();
    break;
  case MODULO:
    if(equals(tokenActual,MODULO))
      eat(MODULO);
    // UNARIO();
    // TERM2();
    TERM();
    break;
  default:
    break;
  }
}
void UNARIO(){
  switch (tokenActual->clase)
  {
  case NEGACION:
    if(equals(tokenActual,NEGACION))
      eat(NEGACION);
    UNARIO();
    break;
  case MENOS:
    if(equals(tokenActual,MENOS))  
      eat(MENOS);
    UNARIO();
    break;
  default:
    printf("On factor %i\n",tokenActual->clase);
    FACTOR();
    break;
  }
}
/*
factor -> (bool) | id localizacion2 | numero | cadena | true | false | id(parametros)
---___----
factor id factor2 | (bool) | numero | cadena | true | false 
factor2 -> localizacion2 | (parametros)
*/
void FACTOR(){
  switch (tokenActual ->clase)
  {
  case PARI:
    if(equals(tokenActual,PARI)) 
      eat(PARI);
    if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){
        BOOLEANO();
    }
    if(equals(tokenActual,PARD)) 
      eat(PARD);
    break;
  case NUM:
    if(equals(tokenActual,NUM) )  
      eat(NUM);
    break;
  case CADENA:
    if(equals(tokenActual,CADENA) )  
      eat(CADENA);
    break;
  case TRUE:
    if(equals(tokenActual,TRUE) )  
      eat(TRUE);
    break;
  case FALSE:
    if(equals(tokenActual,FALSE) )  
      eat(FALSE);
    break;
  case IDS:
    printf("FACTOR-IDS %i",tokenActual->clase);
    if(equals(tokenActual,IDS) )  
      eat(IDS);
    if(equals(tokenActual,PARI))
      FACTOR2();  
    break;
  default:
    break;
  }
}
/*
factor2 -> localizacion2 | (parametros)
----------
factor2 -> ( factor3
factor3 -> bool) localizacion2 | parametros)
*/
void FACTOR2(){
  printf("ON FACTOR2-------------------%i",tokenActual->clase);
  if(equals(tokenActual,PARI))
    eat(PARI);
  if(equals(tokenActual,NEGACION) || 
  equals(tokenActual,MENOS)||
  equals(tokenActual,PARI)||
  equals(tokenActual,IDS)||
  equals(tokenActual,NUM)||
  equals(tokenActual,CADENA)||
  equals(tokenActual,TRUE)||
  equals(tokenActual,FALSE)
  ){
    FACTOR3();
  }
}
//factor3 -> bool) localizacion2 | parametros)
void FACTOR3(){
   if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){
      BOOLEANO();
    }
    if(equals(tokenActual,PARD))
      eat(PARD);
    if(equals(tokenActual,PARI))
      LOCALIZACION2();
}
void PARAMETROS(){
  LISTA_PARAM();
}
/*
lista param → lista param , bool | bool
---------
lista_param -> bool lista_param2
lista_param2 -> , bool lista_param2 | eps
*/
void LISTA_PARAM(){
  if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){
    BOOLEANO();
    if(equals(tokenActual,COMA))
      LISTA_PARAM2();
  }
}

void LISTA_PARAM2(){
  if(equals(tokenActual,COMA))
    eat(COMA);
  
  if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){
      BOOLEANO();
      if(equals(tokenActual,COMA))
        LISTA_PARAM2();
    }
}

/*
*localizacion -> localizacion ( bool ) | id
*---------------
*localizacion -> id localizacion2
*localizacion2 -> (bool) localizacion2 | eps
*/
void LOCALIZACION(){
  if(equals(tokenActual,IDS))
    eat(IDS);
  if(equals(tokenActual,PARI))  
    LOCALIZACION2();
}
void LOCALIZACION2(){
  
  if(equals(tokenActual,PARI))
    eat(PARI);
  if(equals(tokenActual,NEGACION)||
    equals(tokenActual,MENOS)||
    equals(tokenActual,IDS)||
    equals(tokenActual,PARI)||
    equals(tokenActual,NUM)||
    equals(tokenActual,CADENA)||
    equals(tokenActual,TRUE)||
    equals(tokenActual,FALSE)||
    equals(tokenActual, MAYOREQ)||
    equals(tokenActual, MAYORQ)||
    equals(tokenActual, MENOREQ)||
    equals(tokenActual, MENORQ)||
    equals(tokenActual, EQUAL)||
    equals(tokenActual, DIFERENCIA)
    ){
    BOOLEANO();  
    }
  if(equals(tokenActual,PARD))  
    eat(PARD);
  if(equals(tokenActual,COMA))
    LOCALIZACION2();
}



void error(char *msg) {
  printf("msg= %s ",msg);
  printf(": línea %i\n", yylineno);
  exit(1);
}
