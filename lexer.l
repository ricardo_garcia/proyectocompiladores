%{
  #include <stdio.h>
  #include "tokens.h"
  #define YY_DECL token *yylex(void)
%}

%option noyywrap
    /*Ricardo garcía garcía 311008201*/
    /*Ultima modificación al 18-11-2020*/
digito [0-9]
letra [a-zA-Z]
espacio [ \t\n]
esps {espacio}
digito_ {digito}|{digito}+\_{digito}+
entero {digito_}+
exp [Ee][+-]?{entero}+
decimal  (({entero}+\.{entero}*)|(\.{entero}+))
decimalB {decimal}+{exp}?
decimalC {digito}+{exp}
decimalD {decimal}|{decimalB}|{decimalC}
imag {entero}[i]|{decimalD}[i]
ids ({letra}|{digito})
%x cadena
%x literal
%option yylineno
%%

";" {ECHO; return crea_token(PCOMA,"");}
"," {ECHO; return crea_token(COMA,"");}
"(" {ECHO; return crea_token(PARI,"");}
")" {ECHO; return crea_token(PARD,"");}
"func" {ECHO; return crea_token(FUNC,"");}
"int" {ECHO; return crea_token(INT,"");}
"float" {ECHO; return crea_token(FLOAT,"");}
"char" {ECHO; return crea_token(CHAR,"");}
"double" {ECHO; return crea_token(DOUBLE,"");}
"void" {ECHO; return crea_token(VOID,"");}
"=" {ECHO; return crea_token(ASIGNACION,"");}
"{" {ECHO; return crea_token(LLAVEIZQ,"");}
"}" {ECHO; return crea_token(LLAVEDER,"");}
"if" {ECHO; return crea_token(IF,"");}
"else" {ECHO; return crea_token(ELSE,"");}
"while" {ECHO; return crea_token(WHILE,"");}
"do" {ECHO; return crea_token(DO,"");}
"break" {ECHO; return crea_token(BREAK,"");}
"switch" {ECHO; return crea_token(SWITCH,"");}
"case" {ECHO; return crea_token(CASE,"");}
":" {ECHO; return crea_token(DOSPUNTOS,"");}
"default" {ECHO; return crea_token(DEFAULT,"");}
"||" {ECHO; return crea_token(OR,"");}
"&&" {ECHO; return crea_token(AND,"");}
"==" {ECHO; return crea_token(EQUAL,"");}
"!=" {ECHO; return crea_token(DIFERENCIA,"");}
"<" {ECHO; return crea_token(MENORQ,"");}
">" {ECHO; return crea_token(MAYORQ,"");}
"<=" {ECHO; return crea_token(MENOREQ,"");}
">=" {ECHO; return crea_token(MAYOREQ,"");}
"+" {ECHO; return crea_token(MAS,"");}
"-" {ECHO; return crea_token(MENOS,"");}
"*" {ECHO; return crea_token(MULTI,"");}
"/" {ECHO; return crea_token(DIV,"");}
"%" {ECHO; return crea_token(MODULO,"");}
"!" {ECHO; return crea_token(NEGACION,"");}
"true" {ECHO; return crea_token(TRUE,"");}
"false" {ECHO; return crea_token(FALSE,"");}

{decimalD}+ {ECHO; return crea_token_tipo(NUM, yytext, 100);}
{imag}+ {ECHO; return crea_token_tipo(NUM, yytext, 100);}
{entero}+ {ECHO; return crea_token_tipo(NUM, yytext, 100);}
"\"" {BEGIN(cadena);}
<cadena>[^\"\n]* {ECHO; return crea_token_tipo(CADENA, yytext, 101);
}
<cadena>\n {/* */}
<cadena>"\"" {BEGIN(INITIAL);}
"\`" {BEGIN(literal);}
<literal>[^\`\n]* {ECHO; return crea_token_tipo(CADENA, yytext, 101);
}
<literal>\n {/* */}
<literal>"\`" {BEGIN(INITIAL);}
_?{1}{ids}+ {
  ECHO; return crea_token_tipo(IDS, yytext, 103);
}


{espacio} {ECHO;}
<<EOF>> { puts("Fin de análisis léxico."); return crea_token(FIN, ""); }
. { printf("Error léxico en la linea: %i\n", yylineno); exit(1); }

%%


