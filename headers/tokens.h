#ifndef TOKENS_H
#define TOKENS_H
#define PCOMA 1
#define COMA 2
#define PARI 3
#define PARD 4
#define FUNC 5
#define INT 6
#define FLOAT 7
#define CHAR 8
#define DOUBLE 9
#define VOID 10
#define ASIGNACION 11
#define LLAVEIZQ 12
#define LLAVEDER 13
#define IF 14
#define ELSE 15
#define WHILE 16
#define DO 17
#define BREAK 18
#define SWITCH 39
#define CASE 19
#define DOSPUNTOS 20
#define DEFAULT 21
#define OR 22
#define AND 23
#define EQUAL 24
#define DIFERENCIA 25
#define MENORQ 26
#define MAYORQ 27
#define MENOREQ 28
#define MAYOREQ 29
#define MAS 30
#define MENOS 31
#define MULTI 32
#define DIV 33
#define MODULO 34
#define NEGACION 35
#define NEGATIVO 36
#define TRUE 37
#define FALSE 38
#define NUM 100
#define CADENA 101
#define BOOL 102
#define IDS 103
#define FIN 9001
/* 
 * podemos definir más o cambiar las definiciones 
 * siempre y cuando actualicemos el lexer 
 */
#endif

#include <string.h>
#include <stdlib.h>


/**
 * Estructura para modelar tokens (componentes léxicos)
 */
typedef struct token {
  int clase; //léxica
  char *valor; //lexema
  int tipo; //para diferenciar entre diferentes lexemas de una misma clase léxica
} token;

/**
 * Inicialización de tokens sin tipo
 */
token *crea_token(int clase, char *valor);

/**
 * Inicialización de tokens con tipo
 */
token *crea_token_tipo(int clase, char *valor, int tipo);

/**
 * Verificación de token perteneciente a una clase léxica
 */
int equals(token *t1, int clase);

/**
 * Liberación de memoria de un token
 */
void libera(token *t);



