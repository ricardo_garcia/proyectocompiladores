#include "tokens.h"
#include "lista.h"

/* Token actual en la lectura */
token *tokenActual;
/* Dirección disponible en la TT */
int dir;
/* Tabla de símbolos */
Lista *tablaSimbolos;
/* Tabla de tipos */
Lista *tablaTipos;
void executeExp();
void PROGRAMA();
void DECLARACIONES();
void TIPO();
void BASICO();
void COMPUESTO();
void LISTA_VAR();
void LISTA_VAR2();
void FUNCIONES();
void ARGUMENTOS();
void LISTA_ARGS();
void LISTA_ARGS2();
void BLOQUE();
void INSTRUCCIONES();
void INSTRUCCIONES2();
void SENTENCIA();
void CASOS();
void CASO();
void PREDETERMINADO();
void BOOLEANO();
void BOOLEANO2();
void COMB();
void COMB2();
void IGUALDAD();
void IGUALDAD2();
void REL();
void EXP();
void EXP2();
void TERM();
void TERM2();
void UNARIO();
void FACTOR();
void FACTOR2();
void FACTOR3();
void PARAMETROS();
void LISTA_PARAM();
void LISTA_PARAM2();
void LOCALIZACION();
void LOCALIZACION2();

/**
 * Función para consumo de componente léxico actual al hacer empate
 */
void eat(int clase);

/**
 * Función principal de análisis sintáctico. 
 * Debe llamar a función de símbolo inicial de la gramática.
 * También inicializar las tablas de símbolos y tipos.
 */
void parse();

/**
 * Función para reporte de errores sintácticos y semánticos.
 */
void error(char *msg);

/**
 * (opcional) Función para buscar símbolos en la tabla de símbolos
 */
int buscar(char *simbolo);

/**
 * Obtener tamaño de elemento de la tabla de tipos
 */
int getTam(int id);

/**
 * Función para imprimir Tabla de tipos
 */
void printTT(Lista *TT);

/**
 * Función para imprimir Tabla de símbolos
 */
void printTS(Lista *TS);


